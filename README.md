# ConferenceGo

- Riley Brosnan

# Intended market

Currently targeting conference planners who are looking to schedule amd maintain conferences and user information.

# Functionality

- Conference Planners can add conferences
    - Automatically generated picture of conference city
    - Automatically generate weather
    - See attendees of conference
- Conference Goers can preview conferences
    - Sign Up for conferences
    - View description of conferences
    - View Weather Forecast of conferences


# Tech Stack

- [![React][React.js]][React-url]
- [![Django][Django.project]][Django-url]
- [![Docker][Docker.dev]][Docker-url]
- [![JavaScript][JavaScript.js]][JavaScript-url]
- [![Python][Python.py]][Python-url]


## Project Initialization


To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Fork the repository
2. Clone the repository down to your local machine
3. CD into the new project directory
4. Create .env based off of [example.env](example.env)
5. Run `docker compose build`
6. Run `docker compose up`
7. Navigate to [http://localhost:3001/](http://localhost:3001/)
8. Exit the container's CLI, and enjoy


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Docker.dev]: https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/
[JavaScript.js]: https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E
[JavaScript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[Python.py]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54
[Python-url]: https://www.python.org/
[Django.project]: https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com/
